package pl.promity.activeobjects.service;

import java.util.ArrayList;
import java.util.List;

import pl.promity.activeobjects.entity.Animal;
import pl.promity.activeobjects.entity.Bird;
import pl.promity.activeobjects.entity.Cat;
import pl.promity.activeobjects.entity.Fish;

import com.atlassian.activeobjects.external.ActiveObjects;

import static com.google.common.collect.Lists.newArrayList;

public class AnimalServiceImpl implements AnimalService {

	private final ActiveObjects ao;

	public AnimalServiceImpl(ActiveObjects ao) {
		this.ao = ao;
	}

	// FISH
	@Override
	public Fish addFish(String name, int age, long property, Long projectId) {
		
		final Fish fish = ao.create(Fish.class);
				
		setFields(fish, name, age, projectId);
		fish.setProperty1(property);
		fish.save();

		return fish;
	}


	@Override
	public List<Fish> allFish() {
		return newArrayList(ao.find(Fish.class)); // sprawdzic czy mozna dodac to listy
	}
	

	// BIRD
	@Override
	public Bird addBird(String name, int age, long property, Long projectId) {
		
		final Bird bird = ao.create(Bird.class);
		
		setFields(bird, name, age, projectId);
		bird.setProperty3(property);
		bird.save();

		return bird;
	}


	@Override
	public List<Bird> allBird() {
		
		return newArrayList(ao.find(Bird.class));
	}

	
	// CAT
	@Override
	public Cat addCat(String name, int age, long property, Long projectId) {
		final Cat cat = ao.create(Cat.class);
		
		setFields(cat,name,age,projectId);
		cat.setProperty2(property);
		cat.save();
		
		return cat;
	}


	@Override
	public List<Cat> allCat() {
		return newArrayList(ao.find(Cat.class));
	}
	
	@Override
	public void clearDb() {

		for (Bird b : ao.find(Bird.class)) {

			ao.delete(b);
		}

		for (Fish f : ao.find(Fish.class)) {

			ao.delete(f);
		}

		for (Cat c : ao.find(Cat.class)) {

			ao.delete(c);
		}

	}
	
	private boolean setFields(Animal animal,String name, int age,Long projectId){
		
		animal.setAge(age);
		animal.setProjectId(projectId);
		animal.setName(name);
		
		return true;
	}

}

package pl.promity.activeobjects.service;

import java.util.List;

import pl.promity.activeobjects.entity.Bird;
import pl.promity.activeobjects.entity.Cat;
import pl.promity.activeobjects.entity.Fish;

public interface AnimalService {
	
	Fish addFish(String name,int age,long property1,Long projectId);
	List<Fish> allFish();
		
	Bird addBird(String name,int age,long property2,Long projectId);
	List<Bird> allBird();
	
	Cat addCat(String name,int age,long property1,Long projectId);
	List<Cat> allCat();
	
	void clearDb();
	
}

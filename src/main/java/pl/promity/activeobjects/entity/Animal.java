package pl.promity.activeobjects.entity;

import net.java.ao.Entity;
import net.java.ao.Polymorphic;

// PARENT

@Polymorphic
public interface Animal extends Entity {
	
	
	String getName();
	void setName(String name);
	
	int getAge();
	void setAge(int age);
	
	Long getProjectId();
	void setProjectId(Long projectID);

}

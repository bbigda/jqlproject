package pl.promity.jql;

import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.apache.lucene.util.NumericUtils;
import org.ofbiz.core.entity.GenericEntityException;

import pl.promity.activeobjects.entity.Animal;
import pl.promity.activeobjects.entity.Bird;
import pl.promity.activeobjects.entity.Cat;
import pl.promity.activeobjects.entity.Fish;
import pl.promity.activeobjects.service.AnimalService;

import com.atlassian.crowd.embedded.api.User;
import com.atlassian.jira.JiraDataType;
import com.atlassian.jira.JiraDataTypes;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.IssueManager;
import com.atlassian.jira.jql.operand.QueryLiteral;
import com.atlassian.jira.jql.query.QueryCreationContext;
import com.atlassian.jira.plugin.jql.function.AbstractJqlFunction;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.project.ProjectManager;
import com.atlassian.jira.util.MessageSet;
import com.atlassian.jira.util.MessageSetImpl;
import com.atlassian.jira.util.NotNull;
import com.atlassian.query.clause.TerminalClause;
import com.atlassian.query.operand.FunctionOperand;

public class ProjectsFunction extends AbstractJqlFunction {

	private static final Logger log = LogManager.getLogger("atlassian.plugin");

	ProjectManager projectManager;
	IssueManager issueManager;
	AnimalService animalService;

	List<Long> projectsIds;
	//List<QueryLiteral> literals;

	public ProjectsFunction( ProjectManager projectManager,IssueManager issueManager,AnimalService animalService) {

		this.projectManager = projectManager;
		this.animalService = animalService;
		this.issueManager = issueManager;

		projectsIds = new ArrayList<Long>();

	}

	@Override
	@NotNull
	public JiraDataType getDataType() {

		return JiraDataTypes.ISSUE;
	}

	@Override
	public int getMinimumNumberOfExpectedArguments() {

		return 3;
	}

	@Override
	@NotNull
	public MessageSet validate(User user, @NotNull FunctionOperand operand,@NotNull TerminalClause terminalClause) {

		List<String> args = operand.getArgs();

		MessageSet messages = new MessageSetImpl();

		
		if (args.size() != 3) {

			messages.addErrorMessage("Wrong number of arguments");

		}

		final String animalType = args.get(0);

		if (!animalType.equalsIgnoreCase("Cat") && !animalType.equalsIgnoreCase("Bird") && !animalType.equalsIgnoreCase("Fish")) {

			messages.addErrorMessage("There is no such Animal in database");
		}
		
		try{
			
			final long test = Long.parseLong(args.get((2)));
			
			
		} catch(Exception e){
			
			messages.addErrorMessage("Last argument must be numeric");
			
		}
	
	
		
		
		

		return messages;
	}

	@Override
	@NotNull
	public List<QueryLiteral> getValues(@NotNull QueryCreationContext context,
										@NotNull FunctionOperand operand,
										@NotNull TerminalClause terminalClause) {

		List<QueryLiteral> literals = new LinkedList<QueryLiteral>();
		List<String> Args = operand.getArgs();

		final String argType = Args.get(0); 	// Cat/Bird/Fish
		final String argProperty = Args.get(1); // Age/Property
		final String argValue = Args.get(2);	// long

		if (argType.equalsIgnoreCase("Cat")) {

			List<Cat> cats = animalService.allCat();

			for (Cat cat : cats) {

				mainCheck(cat, argProperty, argValue, operand,literals);
			}

		} else if (argType.equalsIgnoreCase("Bird")) {

			List<Bird> birds = animalService.allBird();

			for (Bird bird : birds) {

				mainCheck(bird, argProperty, argValue, operand,literals);
			}
		} else if (argType.equalsIgnoreCase("Fish")) {

			List<Fish> fishes = animalService.allFish();

			for (Fish fish : fishes) {

				mainCheck(fish, argProperty, argValue, operand,literals);
			}
		}

		return literals;

	}

	private boolean isAge(String userChoice) {

		if (userChoice.equalsIgnoreCase("Age")) {

			return true;

		} else {

			return false;
		}

	}

	private boolean mainCheck(	Animal animal, String argProperty,
								String argValue, FunctionOperand operand,List<QueryLiteral> literals) {

		if (isAge(argProperty)) {

			try {

				checkAge(animal, argValue, operand, literals); // sprawdza wiek i jesli sie zgadza dodaje zwiazane projekty

			} catch (Exception e) {

				log.error("Error during entity age check");
			}

		} else {

			try {

				checkProperty(animal, argValue, operand,literals);

			} catch (Exception e) {

				log.error("Error during entity property check");

			}
		}

		return true;
	}

	// porownoje wartosc argumentu z polem Age z entity animal i jesli sie
	// zgadza dodaje powiazane projekty
	private boolean checkAge(Animal animal, String animalValue,
							 FunctionOperand operand,List<QueryLiteral> literals) throws GenericEntityException {

		if (Integer.parseInt(animalValue) == animal.getAge()) {

			Project project = projectManager.getProjectObj(animal.getProjectId());
			
			List<Long> projectIssuesId = (List<Long>) issueManager.getIssueIdsForProject(project.getId());
			
						
			for(Long issueId : projectIssuesId){
			
				literals.add(new QueryLiteral(operand, issueId));
			
			}

		}

		return true;

	}

	private boolean checkProperty(Animal animal, String argValue,
			FunctionOperand operand, List<QueryLiteral> literals) throws NullPointerException, GenericEntityException {

		Long entityValue = null;

		// pola Property nie sa dziedziczone z Animal
		if (animal instanceof Fish) 	 {entityValue = ((Fish) animal).getProperty1();}
		else if (animal instanceof Cat)  {entityValue = ((Cat) animal).getProperty2();}
		else if (animal instanceof Bird) {entityValue = ((Bird) animal).getProperty3();
		}

		if (Long.parseLong(argValue) == entityValue) {

			Project project = projectManager.getProjectObj(animal.getProjectId());
			
			List<Long> projectIssuesId = (List<Long>) issueManager.getIssueIdsForProject(project.getId());
			
						
			for(Long issueId : projectIssuesId){
			
				literals.add(new QueryLiteral(operand, issueId));
			
			}

		}

		return true;
	}

}

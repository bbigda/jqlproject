package pl.promity.webwork;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import pl.promity.activeobjects.service.AnimalService;

public final class AnimalServlet  extends HttpServlet {
	
	private final AnimalService as;
	
	public AnimalServlet(AnimalService as){
		
		this.as = as;
	}

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse res)
			throws ServletException, IOException {
		
		final PrintWriter w = res.getWriter();
		w.write("Simple servlet");
		w.close();
		
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse res)
			throws ServletException, IOException {
		
		
        res.sendRedirect(req.getContextPath() + "/plugins/servlet/tutorial/animal");

		
		
		
	}
	
	

}

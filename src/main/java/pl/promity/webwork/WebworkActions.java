package pl.promity.webwork;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import pl.promity.activeobjects.entity.Cat;
import pl.promity.activeobjects.service.AnimalService;

import com.atlassian.jira.project.Project;
import com.atlassian.jira.project.ProjectManager;
import com.atlassian.jira.web.action.JiraWebActionSupport;

public class WebworkActions extends JiraWebActionSupport {

	// dependency injection
	private final AnimalService animalService;
	private final ProjectManager projectManager;
	
	private Map<String, Long> projectsMap;

	private String kind;

	public WebworkActions(AnimalService as, ProjectManager projectManager) {

		this.animalService = as;
		this.projectManager = projectManager;

		projectsMap = new HashMap<String, Long>();

	}


	public String doExecute() throws Exception {

		List<Project> projectList = projectManager.getProjectObjects();

		for (Project p : projectList) {

			projectsMap.put(p.getName(), p.getId());
		}

		return "input";

	}

	public String doAddRecord() throws Exception {

		final String select = request.getParameter("select");

		final String name = request.getParameter("name");
		final int age = Integer.parseInt(request.getParameter("age"));
		final int property = Integer.parseInt(request.getParameter("property"));

		final Long projectId = Long.parseLong(request.getParameter("project_id"));

		if (select.equals("cat")) {

			
			animalService.addCat(name, age, property, projectId);

		} else if (select.equals("fish")) {

			
			animalService.addFish(name, age, property, projectId);
			
		} else if (select.equals("bird")) {

			
			animalService.addBird(name, age, property, projectId);

		}
		
		kind = select;

		return "success";

	}

	public String doClearDb() throws Exception {

		animalService.clearDb();

		return "clear";
	}
	
	public String doTest(){
		
		List<Cat> list = animalService.allCat();
		Cat cat = animalService.addCat("XXX", 99, 999, 0000L);
		
		list.add(cat);
		list.add(cat);
		
		for(Cat c : list){
			
			log.error(""+c.getName());
		}
		
		return "success";
	}


	
	public Map<String, Long> projectsMap() {

		return projectsMap;
	}
	
	public String getKind() {

		return kind;
	}
	
}